```shell
yarn add @vechain.energy/connex-utils
```


## `getConnex`

A simple wrapper that adds support for multiple nodes. Switches automatically between NodeJS and regular Browser-Javascript.

Example without `getConnex` in Browser-Environment:

```ts
import { Connex } from '@vechain/connex'

const connex = new Connex({
  node: 'https://mainnet.veblocks.net',
  network: 'main'
})
```

Example with a single node using `getConnex`:

```ts
import { getConnex } from '@vechain.energy/connex-utils'

const connex = await getConnex({
  node: 'https://mainnet.veblocks.net'
})
```

* The `network` parameter is _optional_, the genesis block is detected from the given node

Example with added functionality of multiple nodes:

```ts
import { getConnex } from '@vechain.energy/connex-utils'

const connex = await getConnex({
  node: [
    'https://invalid.url',
    'https://mainnet.veblocks.net',
    'https://node.vechain.energy'
  ],
  network: 'main'
})
```

* It tests each node url before creating a Connex instance
* The last working node is remembered to not re-test previous failures

Example in NodeJS:

```js
const { getConnex } = require('@vechain.energy/connex-utils')

async function main () {
  const connex = await getConnex({
    node: [
      'https://invalid.url',
      'https://mainnet.veblocks.net',
      'https://node.vechain.energy'
    ]
  })
  const data = await connex.thor.block(10000000).get()
  console.log(data)
}

main()
  .then(() => process.exit(0))
  .catch(err => console.error(err))
```

### Notes

Reasons for the approach:

1. Modifying `Connex` to support multiple nodes would require a bigger refactoring because it creates a network instance and provides no point of validation before using it
2. Creating a `connex` object and doing a test request works but it will loop forever for invalid nodes, there is no disconnect option
3. re-using `SimpleNet` to test a connection failed because of context (`this`) issues, switching to bent was the fastest path for success and might switch to use `axios` (which is used within `SimpleNet`) to reduce project size


## Name Helpers

To support vet.domains name handling there are some helper functions

## `getAddress(name, connex)`

- Helper to always get an address returned, suited for sending integrations.
- Returns given address or a `getRecord()` call.

```js
const { getAddress } = require('@vechain.energy/connex-utils')

// ..

// always get an address
await getAddress("0x981ebf8F1F98465F93fd0208a0b5e531DdC37815", connex) 
await getAddress("hello.vet", connex) 
```


## `getRecord(name, connex)`

- Lookup the address record for a name manually.
- Returns address or throws if no resolver or record is set

```js
const { getRecord } = require('@vechain.energy/connex-utils', connex)

// ..

// get the address for a .vet name
await getRecord("hello.vet", connex) 
```


## `getName(name, connex)`

- Lookup the primary name for an address.
- Returns name or empty string.

```js
const { getName } = require('@vechain.energy/connex-utils')

// ..

// get the primary name for an address
await getName("0x981ebf8F1F98465F93fd0208a0b5e531DdC37815", connex)
```
