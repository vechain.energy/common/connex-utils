export interface ExtendedOptions {
  node: string | string[]
  network?: 'main' | 'test' | Connex.Thor.Block
  noV1Compat?: boolean
}
